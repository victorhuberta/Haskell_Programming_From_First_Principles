module RegisteredUser where

newtype Username =
  Username String
  deriving (Eq, Show)

newtype AccountNumber =
  AccountNumber Integer
  deriving (Eq, Show)

data User =
  UnregisteredUser |
  RegisteredUser Username AccountNumber
  deriving (Eq, Show)


userLogin :: User -> IO ()
userLogin UnregisteredUser = putStrLn "Login failed."
userLogin (RegisteredUser (Username name) (AccountNumber accNum)) =
  putStrLn ("Welcome " ++ name ++ ", your account number is " ++ show accNum)
