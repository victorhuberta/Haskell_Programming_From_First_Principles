module Ex1 where

tensDigit :: Integral a => a -> a
tensDigit x = d
  where xLast = x `div` 10
        d     = xLast `mod` 10 

-- divMod version
tensDigit2 :: Integral a => a -> a
tensDigit2 x = mod (fst . divMod x $ 10) 10

hunsDigit :: Integral a => a -> a
hunsDigit x = mod (div x 100) 10

findDigit :: Integral a => a -> a -> a
findDigit x y = mod (div y x) 10

-- with pattern matching
foldBool3 :: a -> a -> Bool -> a
foldBool3 x y True  = x
foldBool3 x y False = y

-- with case expression
foldBool4 :: a -> a -> Bool -> a
foldBool4 x y z =
  case z of
    True  -> x
    False -> y

-- with guards
foldBool5 :: a -> a -> Bool -> a
foldBool5 x y z
  | z == True  = x
  | z == False = y

g :: (a -> b) -> (a, c) -> (b, c)
g f (x, y) = (f x, y)

roundTrip :: (Show a, Read a) => a -> a
roundTrip = read . show

definitelyDontDoThis :: Bool -> Int
definitelyDontDoThis True = 1
definitelyDontDoThis False = error "oops"

data SumOfThree a b c =
    FirstPossible a
  | SecondPossible b
  | ThirdPossible c
  deriving (Eq, Show)

sumToInt :: SumOfThree a b c -> Integer
sumToInt (FirstPossible _) = 0
sumToInt (SecondPossible _) = 1
sumToInt (ThirdPossible _) = 2
