module Guards where

myAbs :: (Num a, Ord a) => a -> a
myAbs x
  | x < 0     = -x
  | otherwise = x

bloodNa :: (Num a, Ord a) => a -> IO ()
bloodNa x
  | x < 135   = putStrLn "Too Low"
  | x > 145   = putStrLn "Too High"
  | otherwise = putStrLn "Just Right"

isRightTriangle :: (Num a, Eq a) => a -> a -> a -> String
isRightTriangle a b c
  | a^2 + b^2 == c^2 = "YES!"
  | otherwise        = "NOPE"

dogToHumanYrs :: (Num a, Ord a) => a -> a
dogToHumanYrs x
  | x <= 0    = 0
  | x <= 1    = x * 15
  | x <= 2    = x * 12
  | x <= 4    = x * 8
  | otherwise = x * 6

avgGrade :: (Fractional a, Ord a) => a -> Char
avgGrade x
  | y >= 0.9  = 'A'
  | y >= 0.8  = 'B'
  | y >= 0.7  = 'C'
  | y >= 0.6  = 'D'
  | y >= 0.5  = 'E'
  | otherwise = 'F'
  where y = x / 100

pal :: Eq a => [a] -> Bool
pal xs
  | xs == reverse xs = True
  | otherwise        = False

numbers :: (Num a, Ord a) => a -> Int
numbers x
  | x < 0  = -1
  | x == 0 = 0
  | x > 0  = 1
