module CaseExpr where

funcZ :: (Eq a, Num a) => a -> String
funcZ x =
  case x + 1 == 1 of
    True -> "AWESOME"
    False -> "u wot m8"

isPalindrome :: Eq a => [a] -> String
isPalindrome xs =
  case chk of
    True -> "Yes!"
    False -> "Nah"
  where chk = xs == reverse xs

functionC :: Ord a => a -> a -> a
functionC x y =
  case x > y of
    True -> x
    False -> y

ifEvenAdd2 :: Integral a => a -> a
ifEvenAdd2 n =
  case isEven of
    True -> n + 2
    False -> n
  where isEven = mod n 2 == 0

nums :: (Num a, Ord a) => a -> Int
nums x =
  case compare x 0 of
    LT -> -1
    GT -> 1
    _  -> 0 -- or just EQ -> 0
