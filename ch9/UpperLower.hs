module UpperLower where

import Data.Char

uppercasePls :: String -> String
uppercasePls = filter isUpper

capitalize :: String -> String
capitalize (x:xs) = toUpper x : xs

toUppercase :: String -> String
toUppercase []     = []
toUppercase (x:xs) = toUpper x : toUppercase xs

upperHead :: String -> Char
upperHead = toUpper . head
