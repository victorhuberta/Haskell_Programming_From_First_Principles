module Caesar where

import Data.Char
import Test.QuickCheck

userInput :: IO ()
userInput = do
  putStrLn "Caesar Cipher\n-----------"
  
  putStr "plain text: "
  plaintext <- getLine
  putStr "key: "
  key <- getLine

  putStrLn $ "result = " ++ (caesar (read key) plaintext)


chrShift :: (Char -> Char) -> Char -> Char -> Int -> Char -> Char
chrShift next start end n x
  | not (isLetter x) = x
  | isUpper x        = shift (toUpper start) (toUpper end) n x
  | otherwise        = shift (toLower start) (toLower end) n x
  where
    shift s e n x = go n x
      where go 0 x = x
            go n x
              | (next x) < s = go (n - 1) e
              | (next x) > e = go (n - 1) s
              | otherwise    = go (n - 1) (next x)

chrShiftRight :: Int -> Char -> Char
chrShiftRight n x = chrShift succ 'a' 'z' n x

chrShiftLeft :: Int -> Char -> Char
chrShiftLeft n x = chrShift pred 'a' 'z' n x

caesar :: Int -> String -> String
caesar n xs = map (chrShiftRight n) xs

uncaesar :: Int -> String -> String
uncaesar n xs = map (chrShiftLeft n) xs

prop_encodeDecode :: Property
prop_encodeDecode = forAll allowedStrings encodeDecode
  where encodeDecode :: String -> Bool
        encodeDecode s = (uncaesar 2 $ caesar 2 s) == s
        allowedStrings = listOf $ elements ['a'..'z']
