module Mapping where

import Data.Bool

-- Maps work on lists.
addOne :: Num a => [a] -> [a]
addOne = map (+1)

-- Fmaps work on Functor instances.
iWantEven :: Integral a => [a] -> [Bool]
iWantEven = fmap (\x -> rem x 2 == 0)

firstOnlyPls :: [(a, b)] -> [a]
firstOnlyPls = map fst

negateThrees :: (Num a, Eq a) => [a] -> [a]
negateThrees = map (\x -> bool x (-x) (x == 3))

multOfThrees :: Integral a => [a] -> [a]
multOfThrees = filter (\x -> rem x 3 == 0)

-- Using filter.
delArticles :: String -> [String]
delArticles = filter (\x -> x /= "a" && x /= "an") . words

-- Using list comprehension.
altDelArticles :: String -> [String]
altDelArticles xs = [x | x <- (words xs), x /= "a" && x /= "an"]
