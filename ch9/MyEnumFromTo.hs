module EnumFromTo where

myEnumFromTo :: (Ord a, Enum a) => a -> a -> [a]
myEnumFromTo start end
  | start > end  = []
  | start == end = [start]
  | otherwise    = start : myEnumFromTo (succ start) end
