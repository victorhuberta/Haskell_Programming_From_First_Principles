module ListComprehension where

listSquare :: Num a => [a] -> [a]
listSquare xs = [x^2 | x <- xs]

evenSqr :: Integral a => [a]
evenSqr = [x | x <- mySqr, rem x 2 == 0]
  where mySqr = listSquare [1..5]

below50over50 :: (Enum a, Ord a, Num a) => [(a, a)]
below50over50 = [(x, y) | x <- mySqr, y <- mySqr, x < 50, y > 50]
  where mySqr = listSquare [1..5]

myElem :: Eq a => a -> [a] -> Bool
myElem a (x:xs)
  | a == x    = True
  | xs == []  = False
  | otherwise = myElem a xs

acronym :: String -> String
acronym xs = [x | x <- xs, elem x ['A'..'Z']]

vowel :: String -> String
vowel xs = [x | x <- xs, elem x "aeiou"]

sqrCube :: (Num a, Enum a, Ord a) => [(a, a)]
sqrCube = [(x, y) | x <- mySqr, y <- myCube, x < 50, y < 50]
  where mySqr  = [x^2 | x <- [1..5]]
        myCube = [x^3 | x <- [1..5]]
