module ListManipulation where

splitWords :: String -> [String]
splitWords [] = []
splitWords (' ':xs) = splitWords xs
splitWords xs =
  takeWhile notspace xs : (splitWords . dropWhile notspace $ xs)
  where notspace = (/=' ')

altSplitWords :: String -> [String]
altSplitWords xs = go xs [] []
  where go :: String -> String -> [String] -> [String]
        go []       word words = word : words
        go (' ':xs) word words = word : go xs [] words
        go (x:xs)   word words = go xs (word ++ [x]) words

splitBy :: Char -> String -> [String]
splitBy sep xs = go xs [] []
  where go :: String -> String -> [String] -> [String]
        go []     phrase phrases = phrase : phrases
        go (x:xs) phrase phrases
          | x == sep             = phrase : go xs [] phrases
          | otherwise            = go xs (phrase ++ [x]) phrases
