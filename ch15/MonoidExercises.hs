module MonoidExercises where

import Data.Monoid
import Test.QuickCheck

data Optional a =
    Nada
  | Only a
  deriving (Eq, Show)

instance Monoid a => Monoid (Optional a) where
  mempty = Nada

  mappend x    Nada = x
  mappend Nada y    = y
  mappend (Only x) (Only y) = Only (x `mappend` y)

newtype First' a =
  First' { getFirst :: Optional a }
  deriving (Eq, Show)

instance Monoid (First' a) where
  mempty = First' Nada
  mappend x (First' Nada) = x
  mappend (First' Nada) y = y
  mappend x    _ = x

instance Arbitrary a => Arbitrary (First' a) where
  arbitrary = frequency [ (1, return $ First' Nada)
                        , (1, fmap (First' . Only) arbitrary)]

newtype Last' a =
  Last' { getLast :: Optional a }
  deriving (Eq, Show)

instance Monoid (Last' a) where
  mempty = Last' Nada
  mappend x (Last' Nada) = x
  mappend (Last' Nada) y = y
  mappend _    y = y

instance Arbitrary a => Arbitrary (Last' a) where
  arbitrary = frequency [ (1, return $ Last' Nada)
                        , (1, fmap (Last' . Only) arbitrary)]

newtype Mem s a =
  Mem {
    runMem :: s -> (a, s)
  }

instance (Eq s, Eq a, Monoid a) => Monoid (Mem s a) where
  mempty = Mem $ \s -> (mempty, s)
  mappend (Mem f1) (Mem f2) =
    Mem $ wrappedFunc
    where wrappedFunc s
            | f1 s == (mempty, s) = f2 s
            | f2 s == (mempty, s) = f1 s
            | otherwise = f1 $ snd (f2 s)

type FirstMappend = First' String
                 -> First' String
                 -> First' String
                 -> Bool

type LastMappend = Last' String
                -> Last' String
                -> Last' String
                -> Bool

prop_monoidAssoc :: (Eq m, Monoid m) => m -> m -> m -> Bool
prop_monoidAssoc x y z = (x <> y) <> z == x <> (y <> z)

prop_monoidLeftId :: (Eq m, Monoid m) => m -> Bool
prop_monoidLeftId x = mappend mempty x == x

prop_monoidRightId :: (Eq m, Monoid m) => m -> Bool
prop_monoidRightId x = mappend x mempty == x

main :: IO ()
main = do
  quickCheck (prop_monoidAssoc :: FirstMappend)
  quickCheck (prop_monoidLeftId :: First' String -> Bool)
  quickCheck (prop_monoidRightId :: First' String -> Bool)
  quickCheck (prop_monoidAssoc :: LastMappend)
  quickCheck (prop_monoidLeftId :: Last' String -> Bool)
  quickCheck (prop_monoidRightId :: Last' String -> Bool)

  let f' = Mem $ \s -> ("hi", s + 1)
  print $ runMem (f' <> mempty) 0
  print $ runMem (mempty <> f') 0
  print $ (runMem mempty 0 :: (String, Int))
  print $ runMem (f' <> mempty) 0 == runMem f' 0
  print $ runMem (mempty <> f') 0 == runMem f' 0
