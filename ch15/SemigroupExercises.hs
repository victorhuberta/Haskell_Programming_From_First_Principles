module SemigroupExercises where

import Data.Semigroup
import Test.QuickCheck

data Trivial = Trivial deriving (Eq, Show)

instance Semigroup Trivial where
  _ <> _ = Trivial

instance Arbitrary Trivial where
  arbitrary = return Trivial

newtype BoolConj =
  BoolConj Bool
  deriving (Eq, Show)

instance Semigroup BoolConj where
  _ <> (BoolConj False) = BoolConj False
  (BoolConj False) <> _ = BoolConj False
  _ <> _ = BoolConj True

newtype BoolDisj =
  BoolDisj Bool
  deriving (Eq, Show)

instance Semigroup BoolDisj where
  _ <> (BoolDisj True) = BoolDisj True
  (BoolDisj True) <> _ = BoolDisj True
  _ <> _ = BoolDisj False

data Or a b =
    Fst a
  | Snd b
  deriving (Eq, Show)

instance Semigroup (Or a b) where
  (Snd x) <> _ = Snd x
  _ <> (Snd x) = Snd x
  _ <> (Fst x) = Fst x

newtype Combine a b =
  Combine { unCombine :: a -> b }

instance Semigroup b => Semigroup (Combine a b) where
  (Combine f1) <> (Combine f2) = Combine (\x -> (f1 x) <> (f2 x))

newtype Comp a =
  Comp { unComp :: a -> a }

instance Semigroup (Comp a) where
  (Comp f1) <> (Comp f2) = Comp (f1 . f2)

data Validation a b =
  Failure' a | Success' b
  deriving (Eq, Show)

instance Semigroup a => Semigroup (Validation a b) where
  (Failure' x) <> (Failure' y) = Failure' (x <> y)
  (Failure' x) <> _           = Failure' x
  _           <> (Failure' y) = Failure' y
  _           <> (Success' y) = Success' y

newtype AccumulateRight a b =
  AccumulateRight (Validation a b)
  deriving (Eq, Show)

instance Semigroup b => Semigroup (AccumulateRight a b) where
  (AccumulateRight (Success' x)) <> (AccumulateRight (Success' y)) =
    AccumulateRight (Success' (x <> y))
  (AccumulateRight (Failure' x)) <> _ = AccumulateRight (Failure' x)
  _ <> (AccumulateRight (Failure' x)) = AccumulateRight (Failure' x)

newtype AccumulateBoth a b =
  AccumulateBoth (Validation a b)
  deriving (Eq, Show)

instance (Semigroup a, Semigroup b) => Semigroup (AccumulateBoth a b) where
  (AccumulateBoth (Success' x)) <> (AccumulateBoth (Success' y)) = 
    AccumulateBoth (Success' (x <> y))
  (AccumulateBoth vx) <> (AccumulateBoth vy) = AccumulateBoth (vx <> vy)

semigroupAssoc :: (Eq s, Semigroup s) => s -> s -> s -> Bool
semigroupAssoc x y z = (x <> y) <> z == x <> (y <> z)

type TrivialAssoc = Trivial
                 -> Trivial
                 -> Trivial
                 -> Bool

main :: IO ()
main = do
  quickCheck (semigroupAssoc :: TrivialAssoc)
