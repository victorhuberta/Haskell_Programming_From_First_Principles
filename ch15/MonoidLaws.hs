module MonoidLaws where

import Control.Monad
import Data.Monoid
import Test.QuickCheck

data Bull =
    Fulse
  | Twoo
  deriving (Eq, Show)

instance Arbitrary Bull where
  arbitrary = elements [Fulse, Twoo]

instance Monoid Bull where
  mempty = Fulse
  mappend Fulse y     = y
  mappend x     Fulse = x
  mappend _     _     = Twoo

prop_monoidAssoc :: (Eq m, Monoid m) => m -> m -> m -> Bool
prop_monoidAssoc x y z = x <> (y <> z) == (x <> y) <> z

prop_monoidLeftId :: (Eq m, Monoid m) => m -> Bool
prop_monoidLeftId x = mempty <> x == x

prop_monoidRightId :: (Eq m, Monoid m) => m -> Bool
prop_monoidRightId x = x <> mempty == x

type BullAssoc = Bull -> Bull -> Bull -> Bool

main :: IO ()
main = do
  quickCheck (prop_monoidAssoc :: BullAssoc)
  quickCheck (prop_monoidLeftId :: Bull -> Bool)
  quickCheck (prop_monoidRightId :: Bull -> Bool)
