module Aspatterns where

import Data.Char

f :: Show a => (a, b) -> IO (a, b)
f t@(a, _) = do
  print a
  return t

doubleHead :: [a] -> [a]
doubleHead []       = []
doubleHead xs@(x:_) = x : xs

isSubsequenceOf :: Eq a => [a] -> [a] -> Bool
isSubsequenceOf []     _  = True
isSubsequenceOf _      [] = False
isSubsequenceOf (x:xs) ys = (elem x ys) && isSubsequenceOf xs ys 

capitalizeWords :: String -> [(String, String)]
capitalizeWords = map cap . words
  where cap s@(x:xs) = (s, toUpper x : xs)
