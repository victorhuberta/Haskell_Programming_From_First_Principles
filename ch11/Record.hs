module Record where

-- Don't do something like this.
data OldAutomobile =
    OldNull
  | OldCar {
      oldMake :: String, -- :: Automobile -> String
      oldModel :: String,
      oldYear :: Integer
    }
  deriving (Eq, Show)
-- The above causes bottom to propagate around.
-- Consider this: make Null --> Runtime Error

-- Separate the record data constructor like this instead.
data Car =
  Car {
    make :: String, -- :: Car -> String
    model :: String,
    year :: Integer
  }
  deriving (Eq, Show)

data Automobile =
    Null -- the Null is bad though, use Maybe instead.
  | Automobile Car
  deriving (Eq, Show)
-- The above avoids runtime errors!
-- Consider this: make Null --> Type Error

isYear2000 :: Automobile -> Bool
isYear2000 (Automobile (Car _ _ 2000)) = True
isYear2000 _                           = False
