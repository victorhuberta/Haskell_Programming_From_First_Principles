module BinaryTree where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Show)

insertBT :: Ord a => a -> BinaryTree a -> BinaryTree a
insertBT x Leaf = Node Leaf x Leaf
insertBT x (Node left y right)
  | x == y = Node left y right
  | x < y  = Node (insertBT x left) y right
  | x > y  = Node left y (insertBT x right)

mapBT :: (a -> b) -> BinaryTree a -> BinaryTree b
mapBT f Leaf = Leaf
mapBT f (Node left x right) =
  Node (mapBT f left) (f x) (mapBT f right)

preorderBT :: BinaryTree a -> [a]
preorderBT Leaf = []
preorderBT (Node left x right) =
  (x : preorderBT left) ++ (preorderBT right)

inorderBT :: BinaryTree a -> [a]
inorderBT Leaf = []
inorderBT (Node left x right) =
  (inorderBT left) ++ [x] ++ (inorderBT right)

postorderBT :: BinaryTree a -> [a]
postorderBT Leaf = []
postorderBT (Node left x right) =
  (postorderBT left) ++ (postorderBT right) ++ [x]

-- This function isn't correct. I'll be back!
levelorderBT :: BinaryTree a -> [a]
levelorderBT Leaf = []
levelorderBT (Node left x right) =
  x : go (zip (levelorderBT left) (levelorderBT right))
  where go [] = []
        go (x:xs) = fst x : snd x : go xs

foldTree :: (a -> b -> b) -> b -> BinaryTree a -> b
foldTree f start Leaf = start
foldTree f start (Node left x right) =
  foldTree f (f x (foldTree f start left)) right

-- let bt = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)
--
-- Recursive steps:
-- foldTree (+) ((+) 2 (foldTree (+) 0 (Node Leaf 1 Leaf))) (Node Leaf 3 Leaf)
-- foldTree (+) ((+) 3 ((+) 2 (foldTree (+) (foldTree (+) 0 (Node Leaf 1 Leaf)) Leaf) Leaf
-- (+) 3 ((+) 2 (foldTree (+) (foldTree (+) 0 (Node Leaf 1 Leaf)) Leaf)
-- (+) 3 ((+) 2 (foldTree (+) 0 (Node Leaf 1 Leaf)))
-- (+) 3 ((+) 2 (foldTree (+) ((+) 1 (foldTree (+) 0 Leaf)) Leaf))
-- (+) 3 ((+) 2 ((+) 1 (foldTree (+) 0 Leaf)))
-- (+) 3 ((+) 2 ((+) 1 (0)))
-- (+) 3 ((+) 2 (1))
-- (+) 3 (2)
-- 5

-- This mapTree uses foldTree to do its work.
-- TODO: Complete it!
--
-- mapTree' :: (Ord a, Ord b) => (a -> b) -> BinaryTree a -> BinaryTree b
-- mapTree' f = foldTree (
--   \x acc -> if x > (root acc)
--             then Node Leaf (f x) acc
--             else Node acc (f x) Leaf
--   ) Leaf

root :: BinaryTree a -> a
root Leaf         = error "no root"
root (Node _ x _) = x

testBt :: Num a => BinaryTree a
testBt =
  Node (
    Node (
      Node Leaf 3 Leaf) 8 (Node Leaf 10 Leaf)
    ) 12 (
    Node (
      Node Leaf 15 Leaf) 20 (Node Leaf 44 Leaf)
    )

mapEvenExpectedBt :: BinaryTree Bool
mapEvenExpectedBt =
  Node (
    Node (
      Node Leaf False Leaf) True (Node Leaf True Leaf)
    ) True (
    Node (
      Node Leaf False Leaf) True (Node Leaf True Leaf)
    )

testMapBT :: IO ()
testMapBT =
  if mapBT even testBt == mapEvenExpectedBt
  then putStrLn "Test passed for mapBT"
  else putStrLn "Test failed for mapBT"

testTree :: Num a => BinaryTree a
testTree = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)

testPreorder :: IO ()
testPreorder =
  if preorderBT testTree == [2, 1, 3]
  then putStrLn "Preorder fine!"
  else putStrLn "Bad news bears."

testInorder :: IO ()
testInorder =
  if inorderBT testTree == [1, 2, 3]
  then putStrLn "Inorder fine!"
  else putStrLn "Bad news bears."

testPostorder :: IO ()
testPostorder =
  if postorderBT testTree == [1, 3, 2]
  then putStrLn "Postorder fine!"
  else putStrLn "postorder failed check"

testLevelorder :: IO ()
testLevelorder =
  if levelorderBT testTree == [2, 1, 3]
  then putStrLn "Levelorder fine!"
  else putStrLn "Bad news bears."

testFoldTree :: IO ()
testFoldTree =
  if foldTree (+) 0 testBt == 112
  then putStrLn "Foldtree fine!"
  else putStrLn "Bad news bears."
