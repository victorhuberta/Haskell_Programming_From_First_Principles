module Algebraic where

-- This is called the Record syntax.
data Person =
  Person {
    name :: String,
    age  :: Int
  }
  deriving (Eq, Show)

-- Record syntax enables us to access the record's fields.
personNameAge :: Person -> (String, Int)
personNameAge p = (name p, age p) -- like this

data OperatingSystem =
    GnuLinux
  | OpenBSD
  | MacOS
  | Windows
  deriving (Eq, Show)

data ProgrammingLanguage =
    Haskell
  | Agda
  | Idris
  | PureScript
  | C
  | Assembly
  deriving (Eq, Show)

data Programmer =
  Programmer {
    os   :: OperatingSystem,
    lang :: ProgrammingLanguage
  }
  deriving (Eq, Show)

brogrammer :: Programmer
brogrammer = Programmer { os = MacOS, lang = Haskell }

programmingWizard :: Programmer
programmingWizard = Programmer { os = GnuLinux, lang = Assembly }

allOperatingSystems :: [OperatingSystem]
allOperatingSystems = [GnuLinux, OpenBSD, MacOS, Windows]

allProgrammingLangs :: [ProgrammingLanguage]
allProgrammingLangs = [Haskell, Agda, Idris, PureScript, C, Assembly]

allProgrammers :: [Programmer]
allProgrammers = go allOperatingSystems allProgrammingLangs
  where go []       _        = []
        go (os:oss) []       = go oss allProgrammingLangs
        go oss      (pl:pls) =
          Programmer { os = head oss, lang = pl } : go oss pls
