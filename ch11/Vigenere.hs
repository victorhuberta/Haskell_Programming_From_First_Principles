module Vigenere where

import Data.Char
import Test.QuickCheck

userInput :: IO ()
userInput = do
  putStrLn "Vigenere Cipher\n-----------"

  putStr "plain text: "
  plaintext <- getLine
  putStr "key: "
  key <- getLine

  putStrLn $ "result = " ++ (vigenere key plaintext)


encodeWithShift :: (Char -> Char -> Char) -> String -> String -> String
encodeWithShift shift key plaintext =
  go (map toLower key) (map toLower plaintext)
  where go _      []   = []
        go []     vs   = go key vs
        go oks@(k:ks) (v:vs)
          | isLetter v = (shift k v) : go ks vs
          | otherwise  = v : go oks vs

vigenere :: String -> String -> String
vigenere = encodeWithShift shiftRight

unvigenere :: String -> String -> String
unvigenere = encodeWithShift shiftLeft

shiftRight :: Char -> Char -> Char
shiftRight n x = chr(s + (mod ((ord x) + (ord n) - 2*s) 26))
  where s = ord 'a'

shiftLeft :: Char -> Char -> Char
shiftLeft n x = chr(s + (mod ((ord x) - (ord n)) 26))
  where s = ord 'a'

prop_encodeDecode :: Property
prop_encodeDecode = forAll allowedStrings encodeDecode
  where encodeDecode :: String -> Bool
        encodeDecode s = (unvigenere "test" $ vigenere "test" s) == s
        allowedStrings = listOf $ elements ['a'..'z']
