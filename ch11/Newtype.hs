{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleInstances #-}

module Newtype where

class TooMany a where
  tooMany :: a -> Bool

class NonNegative a where
  nonNegative :: a -> Bool

instance TooMany Int where
  tooMany x = x > 42

instance NonNegative Int where
  nonNegative x = x > 0

-- It reuses the same instance for NonNegative that Int has.
newtype Goats = Goats Int deriving (Eq, Show, NonNegative)

-- But it has its own custom instance of TooMany.
instance TooMany Goats where
  tooMany (Goats _) = False -- There are never too many goats!

instance TooMany (Int, String) where
  tooMany (x, y) = x > 42 && (length y > 5)

instance TooMany (Int, Int) where
  tooMany (x, y) = (x + y) > 99

instance (Num a, TooMany a) => TooMany (a, a) where
  tooMany (x, y) = tooMany x || tooMany y
