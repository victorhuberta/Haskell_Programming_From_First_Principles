module Nokia where

import Data.Char
import Data.List

newtype Keypad =
  Keypad [Char]
  deriving (Eq, Show)

newtype DaPhone =
  DaPhone [Keypad]
  deriving (Eq, Show)

nokiaPhone :: DaPhone
nokiaPhone =
  DaPhone [
    Keypad ['1'],
    Keypad ['2', 'a', 'b', 'c'],
    Keypad ['3', 'd', 'e', 'f'],
    Keypad ['4', 'g', 'h', 'i'],
    Keypad ['5', 'j', 'k', 'l'],
    Keypad ['6', 'm', 'n', 'o'],
    Keypad ['7', 'p', 'q', 'r', 's'],
    Keypad ['8', 't', 'u', 'v'],
    Keypad ['9', 'w', 'x', 'y', 'z'],
    Keypad ['*', '^'],
    Keypad ['0', '+', '_'],
    Keypad ['#', '.', ',']
  ]

pressed :: Int -> Keypad -> Char
pressed i k@(Keypad xs)
  | i >= length xs = pressed modLen k
  | otherwise      = xs !! i
  where modLen = mod i (length xs)

pressedCount :: Char -> Keypad -> Int
pressedCount c (Keypad ks) = go c ks 0
  where go _ [] _ = -1
        go c (x:xs) n
          | c == x = if n == 0 then length ks else n
          | otherwise = go c xs (n + 1)

normalizeForPhone :: String -> String
normalizeForPhone [] = []
normalizeForPhone (c:s)
  | isUpper c = '^' : toLower c : normalizeForPhone s
  | c == ' '  = '+' : normalizeForPhone s
  | otherwise = c : normalizeForPhone s

type PressCount = Int

chrToKeypress :: Char -> DaPhone -> [(Keypad, PressCount)]
chrToKeypress c (DaPhone ps) = go c ps
  where go _ [] = [] 
        go c (kp@(Keypad ks):ps) =
          if elem c ks
          then (kp, pressedCount c kp) : go c ps
          else go c ps

toKeypresses :: String -> DaPhone -> [(Keypad, PressCount)]
toKeypresses [] _     = []
toKeypresses s  phone = go (normalizeForPhone s) phone
  where go (x:xs) phone =
          (chrToKeypress x phone) ++ (toKeypresses xs phone)

convo :: [String]
convo =
  ["Wanna play 20 questions",
   "Ya",
   "U 1st haha",
   "Lol ok. Have u ever tasted alcohol lol",
   "Lol ya",
   "Wow ur cool haha. Ur turn",
   "Ok. Do u think I am pretty Lol",
   "Lol ya",
   "Haha thanks just making sure rofl ur turn"]

convoToKeypresses :: [String] -> DaPhone -> [[(Keypad, PressCount)]]
convoToKeypresses []     _     = []
convoToKeypresses (s:ss) phone =
  toKeypresses s phone : convoToKeypresses ss phone

totalPressCount :: [(Keypad, PressCount)] -> PressCount
totalPressCount = foldr (\(_, count) acc -> count + acc) 0

removeItems :: Eq a => a -> [a] -> [a]
removeItems _ []     = []
removeItems t (x:xs) =
  case t == x of
    True  -> removeItems t xs
    False -> x : removeItems t xs

itemCount :: Eq a => [a] -> [(a, Integer)]
itemCount []     = []
itemCount (x:xs) =
  go xs x 1 : itemCount (removeItems x xs)
  where go [] t count = (t, count)
        go (y:ys) t count
          | y == t    = go ys t (count + 1)
          | otherwise = go ys t count

mostPopularItem :: Ord a => [a] -> a
mostPopularItem =
  fst . maximumBy (\(_, x) (_, y) -> compare x y) . itemCount

-- A better solution: let lowercase = map toLower
lowercase :: String -> String
lowercase ""    = ""
lowercase (c:s) = toLower c : lowercase s

letterCount :: String -> [(Char, Integer)]
letterCount = itemCount . lowercase . filter isLetter

mostPopularLetter :: String -> Char
mostPopularLetter = mostPopularItem . lowercase . filter isLetter

coolestLetter :: [String] -> Char
coolestLetter = mostPopularLetter . map mostPopularLetter

coolestWord :: String -> String
coolestWord = mostPopularItem . map lowercase . words
