module Infix where

-- Infix operators need to start with colon (:)
-- and be non-alphanumeric.
data Product a b = a :&: b deriving (Eq, Show)

data Modulo a b = a :% b deriving (Eq, Show)
