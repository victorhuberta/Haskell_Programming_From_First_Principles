module Capitalization where

import Data.Char
import Data.List

capitalizeWord :: String -> String
capitalizeWord ""     = ""
capitalizeWord (x:xs) =
  case isLetter x of
    True  -> toUpper x : xs
    False -> x : capitalizeWord xs

altCapWord :: String -> String
altCapWord w
  | null w    = w
  | otherwise = toUpper firstLetter : map toLower others
  where ([firstLetter], others) = splitAt 1 w

capitalizeParagraph :: String -> String
capitalizeParagraph =
  concat . intersperse "." . map capitalizeWord . splitBy '.'

splitBy :: Char -> [Char] -> [[Char]]
splitBy sep xs = go sep xs []
  where go _   []     acc = acc : []
        go s (y:ys) acc
          | s == y    = acc : go s ys ""
          | otherwise = go s ys (acc ++ [y])
