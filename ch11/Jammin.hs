module Jammin where

import Data.List

data Fruit =
    Peach
  | Plum
  | Apple
  | Blackberry
  deriving (Eq, Ord, Show)

data JamJars =
  Jam {
    fruit :: Fruit,
    jarsCount :: Int
  } deriving (Eq, Ord, Show)

row1 = Jam Apple 56
row2 = Jam Blackberry 40
row3 = Jam Apple 22
row4 = Jam Peach 99
row5 = Jam Blackberry 50
allJam = [row1, row2, row3, row4, row5]

reportJarsCount :: [JamJars] -> [Int]
reportJarsCount = map jarsCount

totalJarsCount :: [JamJars] -> Int
totalJarsCount = sum . reportJarsCount

mostJars :: [JamJars] -> JamJars
mostJars = maximumBy (\x y -> compare (jarsCount x) (jarsCount y))

sortJarsByFruit :: [JamJars] -> [JamJars]
sortJarsByFruit = sortBy (\x y -> compare (fruit x) (fruit y))

groupJarsByFruit :: [JamJars] -> [[JamJars]]
groupJarsByFruit = groupBy (\x y -> (fruit x) == (fruit y)) . sortJarsByFruit
