module HuttonsRazor where

data Expr = Lit Integer | Add Expr Expr

testExpr :: Expr
testExpr = Add (Add (Lit 2) (Lit 3)) (Add (Add (Lit 5) (Lit 7)) (Lit 9))

eval :: Expr -> Integer
eval (Lit x)    = x
eval (Add e e') = (+) (eval e) (eval e')

printExpr :: Expr -> String
printExpr (Lit x)    = show x 
printExpr (Add e e') = concat [printExpr e, " + ", printExpr e']
