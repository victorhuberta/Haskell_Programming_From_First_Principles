module ChapterExercises where

import Data.List

stops :: String
stops = "pbtdkg"

vowels :: String
vowels = "aeiou"

stopVowelStop :: String -> String -> [String]
stopVowelStop ss vs = [[x, y, z] | x <- ss, y <- vs, z <- ss, x == 'p']

nounVerbNoun :: [String] -> [String] -> [String]
nounVerbNoun ns vs = [concat $ intersperse " " [x, y, z] |
  x <- ns, y <- vs, z <- ns]

avgLenOfWords :: Fractional a => String -> a
avgLenOfWords x =
  fromIntegral (sum (map length (words x)))
  /
  fromIntegral (length (words x))

-- Functions below must be written in term of folds.
myAnd :: [Bool] -> Bool
myAnd = foldr (&&) True

myOr :: [Bool] -> Bool
myOr = foldr (||) False

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (\x acc -> f x || acc) False

myElem :: Eq a => a -> [a] -> Bool
myElem x = myAny (==x)

myReverse :: [a] -> [a]
myReverse = foldl (flip (:)) []

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr (\x acc -> f x : acc) []

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f = foldr (\x acc -> if f x then x : acc else acc) []

squish :: [[a]] -> [a]
squish = foldr (++) []

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f = foldr (\x acc -> f x ++ acc) []

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f (x:xs) = foldl (\x y -> if f x y == GT then x else y) x xs

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f (x:xs) = foldl (\x y -> if f x y == LT then x else y) x xs
