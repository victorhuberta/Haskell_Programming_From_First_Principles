module Scanning where

fibo :: Num a => [a]
fibo = 1 : scanl (+) 1 fibo

fibN :: Num a => Int -> a
fibN i = fibo !! i

fib20 :: Num a => [a]
fib20 = take 20 fibo

fib100 :: (Num a, Ord a) => [a]
fib100 = takeWhile (<100) fibo

inf :: a -> [a]
inf x = x : inf x

-- This exercise is stupid.
fact :: Num a => Int -> [a]
fact x = scanl (+) 1 (take (x - 1) $ inf 1)
