module Folding where

concat3Letters :: [String] -> String
concat3Letters = foldr (++) "" . map (take 3)

concat4Letters :: [String] -> String
concat4Letters = foldl (\b a -> (take 4 a) ++ b) ""

theTruth :: Bool
theTruth = foldr f z xs == foldl (flip f) z (reverse xs)
  where f = (:); z = []; xs = [1..5]

myFoldr :: (a -> b -> b) -> b -> [a] -> b
myFoldr f acc []     = acc
myFoldr f acc (x:xs) = f x (myFoldr f acc xs)

myFoldl :: (b -> a -> b) -> b -> [a] -> b
myFoldl f acc []     = acc
myFoldl f acc (x:xs) = myFoldl f (f acc x) xs
