module DbFolding where

import Data.Time

data DatabaseItem =
    DbString String
  | DbNumber Integer
  | DbDate UTCTime
  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [
    DbDate (UTCTime (fromGregorian 1911 5 1) (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbNumber 1001
  , DbDate (UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123))
  ]

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr getDbDate []
  where getDbDate (DbDate x) y = x : y
        getDbDate _          y = y

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr getDbNumber []
  where getDbNumber (DbNumber x) y = x : y
        getDbNumber _            y = y

mostRecent :: [DatabaseItem] -> UTCTime
mostRecent = foldr max start . filterDbDate
  where start = UTCTime (fromGregorian 0 0 0) (secondsToDiffTime 0)

sumDb :: [DatabaseItem] -> Integer
sumDb = sum . filterDbNumber

avgDb :: [DatabaseItem] -> Double
avgDb xs = fromIntegral (sum dbNumbers) / fromIntegral (length dbNumbers)
  where dbNumbers = filterDbNumber xs
