module BadMonad where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes
import Control.Applicative (liftA2)

data CountMe a =
  CountMe Integer a
  deriving (Eq, Show)

instance Functor CountMe where
  fmap f (CountMe i a) = CountMe i (f a)

instance Applicative CountMe where
  pure = CountMe 0
  CountMe n f <*> CountMe n' a = CountMe (n + n') (f a)

instance Monad CountMe where
  CountMe n a >>= f =
    let CountMe n' x = f a
    in CountMe (n + n') x

instance Arbitrary a => Arbitrary (CountMe a) where
  arbitrary = liftA2 CountMe arbitrary arbitrary

instance Eq a => EqProp (CountMe a) where (=-=) = eq

testCountMe :: IO ()
testCountMe = do
  let countMeType = undefined :: CountMe (Int, String, Int)
  quickBatch $ functor countMeType
  quickBatch $ applicative countMeType
  quickBatch $ monad countMeType
