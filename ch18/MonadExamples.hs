module MonadExamples where

import Control.Applicative ((*>))

-- Sequencing actions with syntatic sugar.
sequencing :: IO ()
sequencing = do
  putStrLn "blah"
  putStrLn "another thing"

-- Sequencing actions raw! (for monads)
sequencing' :: IO ()
sequencing' =
  putStrLn "blah" >>
  putStrLn "another thing"

-- Sequencing actions raw! (for applicatives)
sequencing'' :: IO ()
sequencing'' =
  putStrLn "blah" *>
  putStrLn "another thing"

-- Binding with syntatic sugar.
binding :: IO ()
binding = do
  name <- getLine
  putStrLn name

-- Binding it raw!
binding' :: IO ()
binding' =
  getLine >>= putStrLn

bindingAndSequencing :: IO ()
bindingAndSequencing = do
  putStrLn "name pls:"
  name <- getLine
  putStrLn ("y helo thar: " ++ name)

bindingAndSequencing' :: IO ()
bindingAndSequencing' =
  putStrLn "name pls:" >>
  getLine >>= (\name -> putStrLn ("y helo thar: " ++ name))

twoBinds :: IO ()
twoBinds = do
  putStrLn "name pls:"
  name <- getLine
  putStrLn "age pls:"
  age <- getLine
  putStrLn ("y helo thar: "
         ++ name ++ " who is: "
         ++ age ++ " years old.")

twoBinds' :: IO ()
twoBinds' =
  putStrLn "name pls:" >>
  getLine >>=
  \name ->
    putStrLn "age pls:" >>
    getLine >>=
    \age ->
      putStrLn ("y helo thar: "
             ++ name ++ " who is: "
             ++ age ++ " years old.")

twiceWhenEven :: [Integer] -> [Integer]
twiceWhenEven xs = do
  x <- xs
  if even x
  then [x*x, x*x]
  else [x*x]

twiceWhenEven' :: [Integer] -> [Integer]
twiceWhenEven' xs =
  xs >>=
  \x ->
    if even x
    then [x*x, x*x]
    else [x*x]
