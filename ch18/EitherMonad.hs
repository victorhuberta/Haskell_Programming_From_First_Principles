module EitherMonad where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes (monad)

data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)

instance Functor (Sum a) where
  fmap f (First x) = First x
  fmap f (Second x) = Second (f x)

instance Applicative (Sum a) where
  pure x = Second x
  (<*>) (First x)  _          = First x
  (<*>) _          (First x)  = First x
  (<*>) (Second f) (Second x) = Second (f x)

instance Monad (Sum a) where
  return = pure
  (>>=) (First x) _ = First x
  (>>=) (Second x) f = f x

instance (Arbitrary a, Arbitrary b) => Arbitrary (Sum a b) where
  arbitrary = frequency [ (1, First <$> arbitrary)
                        , (1, Second <$> arbitrary) ]

instance (Eq a, Eq b) => EqProp (Sum a b) where (=-=) = eq

testSumMonad :: IO ()
testSumMonad = do
  quickBatch $
    monad (undefined :: Sum (Int, Char, Double) (Int, Char, Double))
