module ChapterExercises where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes
import Control.Applicative (liftA2)

data Nope a =
  NopeDotJpg
  deriving (Eq, Show)

instance Functor Nope where
  fmap _ _ = NopeDotJpg

instance Applicative Nope where
  pure _ = NopeDotJpg
  _ <*> _ = NopeDotJpg

instance Monad Nope where
  _ >>= _ = NopeDotJpg

instance Arbitrary a => Arbitrary (Nope a) where
  arbitrary = return NopeDotJpg

instance EqProp (Nope a) where (=-=) = eq

testNope :: IO ()
testNope = do
  let nopeType = undefined :: Nope (Int, Char, Double)
  quickBatch $ functor nopeType
  quickBatch $ applicative nopeType
  quickBatch $ monad nopeType


data PhhhbbtttEither b a =
    Left' a
  | Right' b
  deriving (Eq, Show)

instance Functor (PhhhbbtttEither b) where
  fmap _ (Right' b) = Right' b
  fmap f (Left' a) = Left' (f a)

instance Applicative (PhhhbbtttEither b) where
  pure = Left'

  (Right' b) <*> _         = Right' b
  _         <*> (Right' b) = Right' b
  (Left' f)  <*> (Left' a)  = Left' (f a)

instance Monad (PhhhbbtttEither b) where
  (Right' b) >>= _ = Right' b
  (Left' a) >>= f = f a

instance (Arbitrary b, Arbitrary a) => Arbitrary (PhhhbbtttEither b a) where
  arbitrary = frequency [ (1, Left' <$> arbitrary)
                        , (1, Right' <$> arbitrary) ]

instance (Eq b, Eq a) => EqProp (PhhhbbtttEither b a) where (=-=) = eq

testPhhhbbtttEither :: IO ()
testPhhhbbtttEither = do
  let targetType = undefined :: PhhhbbtttEither (Int, Char, Int) (Int, Char, Int)
  quickBatch $ functor targetType
  quickBatch $ applicative targetType
  quickBatch $ monad targetType


newtype Identity a =
  Identity a
  deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity (f x)

instance Applicative Identity where
  pure = Identity
  (Identity f) <*> (Identity x) = Identity (f x)

instance Monad Identity where
  return = pure
  (Identity x) >>= f = f x

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = Identity <$> arbitrary

instance Eq a => EqProp (Identity a) where (=-=) = eq

testIdentity :: IO ()
testIdentity = do
  let idType = undefined :: Identity (Int, Char, Double)
  quickBatch $ functor idType
  quickBatch $ applicative idType
  quickBatch $ monad idType


data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap f Nil = Nil
  fmap f (Cons x xs) = Cons (f x) (f <$> xs)

combine :: List a -> List a -> List a
combine Nil         xs' = xs'
combine (Cons x xs) xs' = Cons x $ combine xs xs'

instance Applicative List where
  pure x = Cons x Nil
  Nil       <*> _   = Nil
  _         <*> Nil = Nil
  Cons f fs <*> l   = combine (f <$> l) (fs <*> l)

instance Monad List where
  return = pure
  Nil       >>= _ = Nil
  Cons x xs >>= f = combine (f x) (xs >>= f)

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = frequency [ (1, return Nil)
                        , (1, liftA2 Cons arbitrary arbitrary) ]

instance Eq a => EqProp (List a) where (=-=) = eq

testList :: IO ()
testList = do
  let listType = undefined :: List (Int, Char, Double)
  quickBatch $ functor listType
  quickBatch $ applicative listType
  quickBatch $ monad listType


j :: Monad m => m (m a) -> m a
j = (>>= id)

l1 :: Monad m => (a -> b) -> m a -> m b
l1 = (<$>)

l2 :: Monad m => (a -> b -> c) -> m a -> m b -> m c
l2 f x y = f <$> x <*> y

a :: Monad m => m a -> m (a -> b) -> m b
a = flip (<*>)

meh :: Monad m => [a] -> (a -> m b) -> m [b]
meh [] _ = pure []
meh (x:xs) f = (:) <$> f x <*> (meh xs f)

flipType :: Monad m => [m a] -> m [a]
flipType xs = meh xs id
