module Transform where

excl :: [Char] -> [Char]
excl x = x ++ "!"

getFifth :: [Char] -> Char
getFifth x = x !! 4

dropNine :: [Char] -> [Char]
dropNine x = drop 9 x

thirdLetter :: String -> Char
thirdLetter x = x !! 2

letterIndex :: Int -> Char
letterIndex x = "Curry is awesome!" !! x
