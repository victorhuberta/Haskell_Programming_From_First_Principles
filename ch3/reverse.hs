module Reverse where

dumbReverse :: String -> String
dumbReverse x = concat [(drop 9 x), " ", (take 2 $ drop 6 x), " ", (take 5 x)]

main :: IO ()
main = do
  -- print (dumbReverse "Curry is awesome")
  print $ dumbReverse "Curry is awesome"
