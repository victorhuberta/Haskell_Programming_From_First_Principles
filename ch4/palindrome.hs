module Palindrome where

myReverse :: [a] -> [a]
myReverse xs =
  _myReverse xs []
  where _myReverse [] zs = zs
        _myReverse (y:ys) zs = _myReverse ys (y : zs)

myReverse1 :: [a] -> [a]
myReverse1 [] = []
myReverse1 xs = last xs : (myReverse1 $ take (length xs - 1) xs)

isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs = myReverse xs == xs
