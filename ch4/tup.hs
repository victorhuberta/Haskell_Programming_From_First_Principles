module Tup where

mySwap :: (a, b) -> (b, a)
mySwap t = (snd t, fst t)
