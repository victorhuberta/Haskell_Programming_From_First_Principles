module IfThen where

greetIfCool :: String -> IO ()
greetIfCool greet =
  if isCool greet
    then putStrLn "eyyyyy. What's shakin'?"
  else
    putStrLn "pshhhh."
  where
    isCool :: String -> Bool
    isCool greet = greet == "downright frosty yo"
