module GenerateStuff where

import Test.QuickCheck

genBool :: Gen Bool
genBool = choose (False, True)

genOrd :: Gen Ordering
genOrd = elements [EQ, LT, GT]

genChar :: Gen Char
genChar = elements ['a'..'z']

genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
  x <- arbitrary
  y <- arbitrary
  return (x, y)

genThreeple :: (Arbitrary a, Arbitrary b, Arbitrary c) => Gen (a, b, c)
genThreeple = do
  x <- arbitrary
  y <- arbitrary
  z <- arbitrary
  return (x, y, z)

genMaybe :: Arbitrary a => Gen (Maybe a)
genMaybe = do
  x <- arbitrary
  elements [Nothing, Just x]

genEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
genEither = do
  x <- arbitrary
  y <- arbitrary
  elements [Left x, Right y]

biasedGenMaybe :: Arbitrary a => Gen (Maybe a)
biasedGenMaybe = do
  x <- arbitrary
  frequency [ (1, return Nothing)
            , (3, return (Just x))]
