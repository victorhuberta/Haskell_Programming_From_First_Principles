module Arithmetic where

import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)

inc :: Num a => a -> a
inc = (1+)

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom
  | denom == 0 = error "divided by zero"
  | otherwise  = go num denom 0
  where go n d count
          | n < d     = (count, n)
          | otherwise = go (n - d) d (count + 1)

mult :: (Ord a, Num a) => a -> a -> a
mult a b
  |    (a >= 0 && b >= 0)
    || (a <= 0 && b <= 0) = goAbs
  | otherwise             = negate $ goAbs
  where go x y
          | x == 0 || y == 0 = 0
          | x == 1           = y
          | y == 1           = x
          | otherwise        = x + (go x (y - 1))
        goAbs = go (abs a) (abs b)

incTestCase :: IO ()
incTestCase = hspec $ do
  describe "Arithmetic.inc" $ do
    it "returns any positive number + 1" $ do
      inc (10 :: Int) `shouldBe` 11

    it "returns any negative number + 1" $ do
      inc (-1 :: Int) `shouldBe` 0

    it "anything + 1 will be greater than itself" $ do
      property $ \x -> inc x > (x :: Int)

dividedByTestCase :: IO ()
dividedByTestCase = hspec $ do
  describe "Arithmetic.dividedBy" $ do
    it "10 divided by 2 is 5" $ do
      dividedBy 10 2 `shouldBe` (5 :: Int, 0)

    it "25 divided by 4 is 6 with a remainder of 1" $ do
      dividedBy 25 4 `shouldBe` (6 :: Int, 1)

    it "throws an exception for anything divided by zero" $ do
      evaluate (dividedBy (50 :: Int) 0) `shouldThrow` anyException

multTestCase :: IO ()
multTestCase = hspec $ do
  describe "Arithmetic.mult" $ do
    it "3 * 9 is 27" $ do
      mult 3 9 `shouldBe` (27 :: Int)

    it "anything * 1 is itself" $ do
      mult 100 1 `shouldBe` (100 :: Int)
    
    it "anything * 0 is 0" $ do
      mult 5 0 `shouldBe` (0 :: Int)

    it "multiplication is commutative" $ do
      property $ \x y -> mult (x :: Int) y == mult y x

    it "negative x * positive y has a negative result" $ do
      mult (-1) (3 :: Int) < 0 `shouldBe` True

    it "negative x * negative y has a positive result" $ do
      mult (-3) (-1 :: Int) > 0 `shouldBe` True

prop_commutativeMult :: Int -> Int -> Bool
prop_commutativeMult x y = mult x y == mult y x

runQc :: IO ()
runQc = quickCheck prop_commutativeMult
