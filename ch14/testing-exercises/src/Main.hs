module Main where

import Test.Hspec
import Test.QuickCheck
import WordNumber (digitToWord, digits, wordNumber)
import QuickChecked
import Idempotence
import Generator (fairFoolGen, unfairFoolGen)

takeSamples :: IO ()
takeSamples = do
  putStrLn "Taking samples from fairFoolGen..."
  sample fairFoolGen
  putStrLn "Taking samples from unfairFoolGen..."
  sample unfairFoolGen

runQc :: IO ()
runQc = quickCheckList allProperties

quickCheckList :: [Property] -> IO ()
quickCheckList []     = do return ()
quickCheckList (x:xs) = do
  quickCheck x
  quickCheckList xs

allProperties :: [Property]
allProperties =
  [ prop_halfIdentity
  , prop_listOrdered
  , prop_plusAssociative
  , prop_plusCommutative
  , prop_quotRem
  , prop_divMod
  , prop_powNotAssociative
  , prop_powNotCommutative
  , prop_reverseReverseId
  , prop_dollarApplicative
  , prop_dotComposition
  , prop_foldrPlusPlus
  , prop_foldrConcat
  , prop_takeLength
  , prop_readShow
  , prop_capitalizeWord
  , prop_sort
  ]

main :: IO ()
main = wordNumberTestCase

wordNumberTestCase :: IO ()
wordNumberTestCase = hspec $ do
  describe "WordNumber.digitToWord" $ do
    it "returns the name of a number" $ do
      digitToWord 0 `shouldBe` Just "Zero"
      digitToWord 1 `shouldBe` Just "One"
      digitToWord 2 `shouldBe` Just "Two"

    it "returns nothing for an unknown number" $ do
      digitToWord (-1) `shouldBe` Nothing
      digitToWord (10) `shouldBe` Nothing

  describe "WordNumber.digits" $ do
    it "returns every digit of a positive number" $ do
      digits 12345 `shouldBe` [1, 2, 3, 4, 5]
      digits 1234 `shouldBe` [1, 2, 3, 4]
      digits 123 `shouldBe` [1, 2, 3]
      digits 12 `shouldBe` [1, 2]
      digits 1 `shouldBe` [1]
      digits 0 `shouldBe` [0]

    it "returns every digit of a negative number" $ do
      let negnumGen :: Gen Int
          negnumGen = elements [-12345..(-1)]
      forAll negnumGen (\x -> digits x `shouldBe` digits (abs x))

  describe "WordNumber.wordNumber" $ do
    it "returns with the format 'number-number-...' for any number" $ do
      wordNumber 12345 `shouldBe` "One-Two-Three-Four-Five"
      wordNumber (-123) `shouldBe` "One-Two-Three"
      wordNumber 0 `shouldBe` "Zero"
