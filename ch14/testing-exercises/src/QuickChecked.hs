module QuickChecked where

import Test.QuickCheck
import Data.List (sort)

half :: Fractional a => a -> a
half x = x / 2

listOrdered :: Ord a => [a] -> Bool
listOrdered = snd . foldr go (Nothing, True)
  where go _ status@(_, False) = status
        go x (Nothing, b)      = (Just x, b)
        go x (Just y, _)       = (Just x, y >= x)

prop_halfIdentity :: Property
prop_halfIdentity = property (\x -> ((*2) $ half x) == (x :: Double))

prop_listOrdered :: Property
prop_listOrdered = forAll (arbitrary :: Gen [Int]) (listOrdered . sort)

prop_plusAssociative :: Property
prop_plusAssociative = property associative
  where associative :: Int -> Int -> Int -> Bool
        associative x y z = (x + y) + z == x + (y + z)

prop_plusCommutative :: Property
prop_plusCommutative = property commutative
  where commutative :: Int -> Int -> Bool
        commutative x y = x + y == y + x

prop_quotRem :: Property
prop_quotRem = property quotRemProp
  where quotRemProp :: Int -> Int -> Bool
        quotRemProp x y
          | y == 0    = True
          | otherwise = (quot x y)*y + (rem x y) == x

prop_divMod :: Property
prop_divMod = property divModProp
  where divModProp :: Int -> Int -> Bool
        divModProp x y
          | y == 0    = True
          | otherwise = (div x y)*y + (mod x y) == x

prop_powNotAssociative :: Property
prop_powNotAssociative = expectFailure associative
  where associative :: Int -> Int -> Int -> Bool
        associative x y z = x ^ (y ^ z) == (x ^ y) ^ z

prop_powNotCommutative :: Property
prop_powNotCommutative = expectFailure commutative
  where commutative :: Int -> Int -> Bool
        commutative x y = x ^ y == y ^ x

prop_reverseReverseId :: Property
prop_reverseReverseId = property equalsIdentity
  where equalsIdentity :: [Int] -> Bool
        equalsIdentity xs = reverse (reverse xs) == xs

prop_dollarApplicative :: Property
prop_dollarApplicative = property applicative
  where applicative :: Int -> Bool
        applicative x = (id $ x) == id x

prop_dotComposition :: Property
prop_dotComposition = property composition
  where composition :: Int -> Bool
        composition x = (id . id) x == (\y -> id (id y)) x

prop_foldrPlusPlus :: Property
prop_foldrPlusPlus = property fppProp
  where fppProp :: [Int] -> Bool
        fppProp xs = foldr (:) [] xs == (++) xs []

prop_foldrConcat :: Property
prop_foldrConcat = property fcProp
  where fcProp :: [[Int]] -> Bool
        fcProp xs = foldr (++) [] xs == concat xs

prop_takeLength :: Property
prop_takeLength = expectFailure takeLength
  where takeLength :: Int -> [Int] -> Bool
        takeLength n xs = length (take n xs) == n

prop_readShow :: Property
prop_readShow = property readShow
  where readShow :: Int -> Bool
        readShow x = read (show x) == x
