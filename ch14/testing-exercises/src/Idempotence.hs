module Idempotence where

-- Idempotence refers to the property of a function in
-- which the result value does not change beyond the
-- initial application.

import Data.Char (toUpper, isLetter)
import Data.List (sort)
import Test.QuickCheck (Property, property)

twice :: (a -> a) -> (a -> a)
twice f = f . f

fourTimes :: (a -> a) -> (a -> a)
fourTimes = twice . twice

capitalizeWord :: String -> String
capitalizeWord [] = []
capitalizeWord (c:s)
  | isLetter c = toUpper c : s
  | otherwise  = c : capitalizeWord s

prop_capitalizeWord :: Property
prop_capitalizeWord = property go
  where cap = capitalizeWord
        go :: String -> Bool
        go x =    cap x == (twice cap) x
               && cap x == (fourTimes cap) x

prop_sort :: Property
prop_sort = property go
  where go :: [Int] -> Bool
        go xs =    sort xs == (twice sort) xs
                && sort xs == (fourTimes sort) xs
