module WordNumber where

import Data.List (intersperse)
import Data.Maybe (mapMaybe)

digitToWord :: Int -> Maybe String
digitToWord 0 = Just "Zero"
digitToWord 1 = Just "One"
digitToWord 2 = Just "Two"
digitToWord 3 = Just "Three"
digitToWord 4 = Just "Four"
digitToWord 5 = Just "Five"
digitToWord 6 = Just "Six"
digitToWord 7 = Just "Seven"
digitToWord 8 = Just "Eight"
digitToWord 9 = Just "Nine"
digitToWord _ = Nothing

digits :: Int -> [Int]
digits 0 = [0]
digits n = go (abs n)
  where go 0 = []
        go x = go (div x 10) ++ [mod x 10]

wordNumber :: Int -> String
wordNumber = concat . intersperse "-" . mapMaybe digitToWord . digits
