module Generator where

import Test.QuickCheck

data Fool = Fulse | Frue deriving (Eq, Show)

fairFoolGen :: Gen Fool
fairFoolGen = elements [Fulse, Frue]

unfairFoolGen :: Gen Fool
unfairFoolGen =
  frequency [ (2, return Fulse)
            , (1, return Frue) ]
