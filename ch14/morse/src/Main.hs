module Main where

import Control.Monad (forever, when)
import Data.List (intercalate)
import Data.Traversable (traverse)
import Morse (stringToMorse, morseToChar)
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess)
import System.IO (hGetLine, hIsEOF, stdin)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [arg] ->
      case arg of
        "from" -> convertFromMorse
        "to"   -> convertToMorse
        _      -> argError

    _     -> argError
    where argError = do
            putStrLn "Please specify the first argument\
                      \ as being 'from' or 'to' morse,\
                      \ such as: morse to"
            exitFailure

convertToMorse :: IO ()
convertToMorse = forever $ do
  weAreDone <- hIsEOF stdin
  when weAreDone exitSuccess

  line <- hGetLine stdin
  case stringToMorse line of
    (Just morse) -> putStrLn $ intercalate " " morse
    Nothing      -> do
      putStrLn ("No Morse Code for: " ++ line)
      exitFailure

convertFromMorse :: IO ()
convertFromMorse = forever $ do
  weAreDone <- hIsEOF stdin
  when weAreDone exitSuccess

  line <- hGetLine stdin
  case traverse morseToChar (words line) of
    (Just morse) -> putStrLn morse
    Nothing      -> do
      putStrLn ("Invalid Morse Code: " ++ line)
      exitFailure
