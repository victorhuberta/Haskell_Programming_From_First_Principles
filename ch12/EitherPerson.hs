module EitherPerson where

type Name = String
type Age = Int

data Person = Person Name Age deriving (Eq, Show)
data PersonInvalid = NameEmpty | AgeTooLow deriving (Eq, Show)

type ValidatePerson a = Either [PersonInvalid] a

nameOk :: Name -> Either [PersonInvalid] Name
nameOk "" = Left [NameEmpty]
nameOk n  = Right n

ageOk :: Age -> Either [PersonInvalid] Age
ageOk x
  | x <= 0    = Left [AgeTooLow]
  | otherwise = Right x

mkPerson :: Name -> Age -> ValidatePerson Person
mkPerson name age = mkPerson' (nameOk name) (ageOk age)

mkPerson' :: ValidatePerson Name
          -> ValidatePerson Age
          -> ValidatePerson Person
mkPerson' (Right name)   (Right age)   = Right (Person name age)
mkPerson' (Left nameErr) (Left ageErr) = Left (nameErr ++ ageErr)
mkPerson' (Left nameErr) _             = Left nameErr
mkPerson' _              (Left ageErr) = Left ageErr
