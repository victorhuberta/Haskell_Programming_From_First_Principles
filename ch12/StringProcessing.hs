module StringProcessing where

import Data.List
import Data.Char

notThe :: String -> Maybe String
notThe s =
  case any (\w -> w == "the") $ words s of
    True  -> Nothing
    False -> Just s

replace :: Eq a => a -> a -> [a] -> [a]
replace o n = map (\x -> if x == o then n else x)

replaceThe :: String -> String
replaceThe s = 
  case notThe s of
    Just _  -> s
    Nothing -> concat $ intersperse " "
                 $ replace "the" "a" $ words s

isVowel :: Char -> Bool
isVowel c = elem c "aieou"

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel s = go (words s)
  where go []     = 0
        go (_:[]) = 0
        go (x:y:xs)
          | x == "the" && isVowel (head y) = 1 + (go (y:xs))
          | otherwise                      = go (y:xs)

returnVowels :: String -> String
returnVowels = filter isVowel

returnConsonants :: String -> String
returnConsonants = filter (\c -> isLetter c && (not $ isVowel c))

countVowels :: String -> Integer
countVowels = foldr (\_ acc -> acc + 1) 0 . returnVowels

countConsonants :: String -> Integer
countConsonants = foldr (\_ acc -> acc + 1) 0 . returnConsonants

newtype Word = Word String deriving (Eq, Show)

validateWord :: String -> Maybe Word
validateWord w
  | countVowels w > countConsonants w = Nothing
  | otherwise                         = Just (Word w)
