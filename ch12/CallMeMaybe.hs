module CallMeMaybe where

ifEvenAdd2 :: Integral a => a -> Maybe a
ifEvenAdd2 x = if even x then Just (x+2) else Nothing

type Name = String
type Age = Int
data Person = Person Name Age deriving (Eq, Show)

-- Smart constructor
mkPerson :: Name -> Age -> Maybe Person
mkPerson name age
  | name == "" || age <= 0 = Nothing
  | otherwise              = Just (Person name age)

isJust :: Maybe a -> Bool
isJust (Just _) = True
isJust _        = False

isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing _       = False

mayybee :: b -> (a -> b) -> Maybe a -> b
mayybee d _ Nothing  = d
mayybee d f (Just x) = f x

fromMaybe :: a -> Maybe a -> a
fromMaybe d Nothing  = d
fromMaybe _ (Just x) = x

listToMaybe :: [a] -> Maybe a
listToMaybe []     = Nothing
listToMaybe (x:xs) = Just x

maybeToList :: Maybe a -> [a]
maybeToList Nothing  = []
maybeToList (Just x) = [x]

catMaybes :: [Maybe a] -> [a]
catMaybes []            = []
catMaybes (Nothing:xs)  = catMaybes xs
catMaybes ((Just x):xs) = x : catMaybes xs

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe xs
  | any isNothing xs = Nothing
  | otherwise        = Just (catMaybes xs)
