module BinaryTreeUnfold where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

unfoldBt :: (a -> Maybe (a, b, a)) -> a -> BinaryTree b
unfoldBt f start = go f (f start)
  where go f Nothing          = Leaf
        go f (Just (x, y, z)) = Node (go f (f x)) y (go f (f z))

treeBuild :: Integer -> BinaryTree Integer
treeBuild 0 = Leaf
treeBuild n =
  unfoldBt (\x -> if x == n then Nothing else Just (x+1, x, x+1)) 0
