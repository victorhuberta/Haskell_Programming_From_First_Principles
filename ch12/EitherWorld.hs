module EitherWorld where

lefts :: [Either a b] -> [a]
lefts []             = []
lefts ((Left x):xs)  = x : lefts xs
lefts (_:xs)         = lefts xs

lefts' :: [Either a b] -> [a]
lefts' = foldr accLefts []
  where accLefts (Left x) acc = x : acc
        accLefts _        acc = acc

rights :: [Either a b] -> [b]
rights []             = []
rights ((Right x):xs) = x : rights xs
rights (_:xs)         = rights xs

rights' :: [Either a b] -> [b]
rights' = foldr accRights []
  where accRights (Right x) acc = x : acc
        accRights _         acc = acc

partitionEithers :: [Either a b] -> ([a], [b])
partitionEithers xs = (lefts xs, rights xs)

eitherMaybe :: (b -> c) -> Either a b -> Maybe c
eitherMaybe f (Right x) = Just (f x)
eitherMaybe _ _         = Nothing

either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' fl _ (Left x) = fl x
either' _ fr (Right x) = fr x

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' f = either' (\_ -> Nothing) (\x -> Just (f x))
