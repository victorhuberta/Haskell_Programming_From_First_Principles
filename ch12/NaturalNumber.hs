module NaturalNumber where

data Natural =
    Zero
  | Succ Natural
  deriving (Eq, Show)

natToInteger :: Natural -> Integer
natToInteger Zero     = 0
natToInteger (Succ n) = 1 + (natToInteger n)

integerToNat :: Integer -> Maybe Natural
integerToNat i
  | i < 0     = Nothing
  | otherwise = Just (go i)
  where go 0 = Zero
        go i = Succ (go (i - 1))
