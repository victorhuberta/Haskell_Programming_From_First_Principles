module Unfolding where

import Data.List

myIterate :: (a -> a) -> a -> [a]
myIterate f acc = acc : myIterate f (f acc)

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f start = go f (f start)
  where go f Nothing       = []
        go f (Just (x, y)) = x : go f (f y)

betterIterate :: (a -> a) -> a -> [a]
betterIterate f = myUnfoldr (\x -> Just (x, f x))

myIterateTests :: [Bool]
myIterateTests = [
    (take 10 $ myIterate (+1) 0) == (take 10 $ iterate (+1) 0),
    (take 10 $ myIterate (subtract 1) 0) ==
      (take 10 $ iterate (subtract 1) 0),
    (take 10 $ myIterate (\(x, y) -> (x, y+1)) (0, 1)) ==
      (take 10 $ iterate (\(x, y) -> (x, y+1)) (0, 1))
  ]

myUnfoldrTests :: [Bool]
myUnfoldrTests = [
    (take 10 $ myUnfoldr (\x -> Just (x, x+1)) 0) ==
      (take 10 $ unfoldr (\x -> Just (x, x+1)) 0),
    (take 10 $ myUnfoldr (\x -> Just (x, x-1)) 0) ==
      (take 10 $ unfoldr (\x -> Just (x, x-1)) 0)
  ]

betterIterateTests :: [Bool]
betterIterateTests = [
    (take 10 $ betterIterate (+1) 0) == (take 10 $ iterate (+1) 0),
    (take 10 $ betterIterate (subtract 1) 0) ==
      (take 10 $ iterate (subtract 1) 0),
    (take 10 $ betterIterate (\(x, y) -> (x, y+1)) (0, 1)) ==
      (take 10 $ iterate (\(x, y) -> (x, y+1)) (0, 1))
  ]

testFunc :: [Bool] -> IO ()
testFunc tests =
  if and tests
  then putStrLn "testFunc: OKAY"
  else putStrLn "testFunc: FAIL"
