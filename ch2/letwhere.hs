-- letwhere.hs

module LetWhere where

printInc2 n = print (addTwo + addThree)
              where addTwo = n + 2
                    addThree = n + 3

printInc3 n = let x = 3; y = 5
              in print (n + x + y)

-- Let's try them in lambda forms.
printInc4 n = (\x -> print (n + x)) 4

printInc5 n = (\x -> print x) (n + 5)

-- let x = 3; y = 1000 in x * 3 + y
lf1 = (\x y -> x * 3 + y) 3 1000

-- let y = 10; x = 10 * 5 + y in x * 5
lf2 = (\y x -> x * 5) y (10 * 5 + y) where y = 10

-- let x = 7; y = negate x; z = y * 10 in z / x + y
lf3 = (\x y z -> z / x + y) x y (y * 10) where x = 7; y = negate x

waxOn = x * 5
    where z = 7
          y = z + 8
          x = y ^ 2
