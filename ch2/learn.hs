module Learn where

-- Definitions order doesn't matter in a source file.
x = 10 * 5 + y

myResult = x * 5

y = 10

area x = 3.14 * (x * x)

double x = x * 2
