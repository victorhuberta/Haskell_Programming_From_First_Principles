sayHello :: String -> IO ()
sayHello x = putStrLn ("Hello, " ++ x ++ "!")

quadruple :: Integer -> Integer
quadruple x = x * 4
