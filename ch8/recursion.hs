module Recursion where

factorial :: Integer -> Integer
factorial 0 = 1
factorial x = x * (factorial (x - 1))

applyTimes :: Integer -> (b -> b) -> b -> b
applyTimes 0 f x = x
applyTimes n f x = f $ applyTimes (n - 1) f x

incTimes :: Num b => Integer -> b -> b
incTimes n x = applyTimes n (+1) x

fiboseq :: Integral a => a -> [a]
fiboseq n = _fiboseq 0 1 n
  where _fiboseq x y n
          | x == n    = []
          | otherwise = [y] ++ (_fiboseq y (x + y) n)

fibonum :: Integral a => a -> a
fibonum 0 = 0
fibonum 1 = 1
fibonum x = fibonum (x - 1) + fibonum (x - 2)

myDiv :: Integral a => a -> a -> a
myDiv x y
  | x < y     = 0
  | otherwise = 1 + (myDiv (x - y) y)

mySum :: Integral a => a -> a
mySum 0 = 0
mySum n
  | n < 0     = n + mySum (n + 1)
  | otherwise = n + mySum (n - 1)

myMult :: Integral a => a -> a -> a
myMult x 0 = 0
myMult x y = x + myMult x (y - 1)

mc91 :: Integral a => a -> a
mc91 n
  | n > 100  = n - 10
  | n <= 100 = mc91 . mc91 $ (n + 11)
