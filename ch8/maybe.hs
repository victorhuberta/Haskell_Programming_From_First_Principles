module Maybe where

data MyNumber =
    MyNumber String
  | FakeNumber String
  deriving (Eq, Read, Show)

myNameIs :: String -> Maybe MyNumber
myNameIs "Victor" = Just (MyNumber "0123456789")
myNameIs "Jesus"  = Just (FakeNumber "9876543210")
myNameIs _        = Nothing
