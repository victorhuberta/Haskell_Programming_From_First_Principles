module MyDivMod where

-- This function's behavior is different from divMod when
-- negative numbers are involved. Ex: myDivMod 10 (-3) -> (-3, -1)
--
myDivMod :: Integral a => a -> a -> (a, a)
myDivMod nom denom 
  |    (nom > 0 && denom < 0)
    || (nom < 0 && denom > 0) = (-(fst primaryFunc), -(snd primaryFunc))
  | denom == 0                = error "divide by zero"
  | otherwise                 = primaryFunc
  where _myDivMod n d count
          | n < d     = (count, n)
          | otherwise = _myDivMod (n - d) d (count + 1)
        primaryFunc = _myDivMod (abs nom) (abs denom) 0
