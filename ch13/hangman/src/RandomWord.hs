module RandomWord where

import System.Random (randomRIO)

newtype Wordlist =
  Wordlist [String]
  deriving (Eq, Show)

randomWord :: IO String
randomWord = gameWords >>= randomWordFrom

randomWordFrom :: Wordlist -> IO String
randomWordFrom (Wordlist wl) = do
  rn <- randomRIO (0, length wl - 1)
  return (wl !! rn)

gameWords :: IO Wordlist
gameWords  = do
  (Wordlist aw) <- allWords
  return $ Wordlist (filter gameWordLength aw)
  where gameWordLength w =
          let l = length w
          in l >= gameWordMinLen && l <= gameWordMaxLen

gameWordMaxLen :: Int
gameWordMaxLen = 8

gameWordMinLen :: Int
gameWordMinLen = 3

allWords :: IO Wordlist
allWords = do
  dict <- readFile dictFile
  return $ Wordlist (lines dict)

dictFile :: String
dictFile = "data/linux.words"
