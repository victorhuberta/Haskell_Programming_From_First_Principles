module HangmanGame where

import Control.Monad (forever)
import System.Exit (exitSuccess)
import Data.Maybe (isJust)
import Puzzle

runGame :: Puzzle -> IO ()
runGame puzzle@(Puzzle word _ _) = forever $ do
  gameWin puzzle
  gameOver puzzle
  putStrLn (show puzzle)
  putStr "Guess a letter: "
  guess <- getLine
  case guess of
    "godmode" -> do putStrLn ("WORD=" ++ word)
                    runGame puzzle
    [c] -> handleGuess puzzle c >>= runGame
    _   -> putStrLn "Your guess must be\
                     \ a single character!"

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess =
  case (charInWord puzzle guess,
        alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You've already guessed that, dumbass!"
      return puzzle
    (True, _) -> do
      putStrLn "Aw yissss, you got one!"
      return (fillInChar puzzle guess)
    (False, _) -> do
      putStrLn "Nah, you stupid bitch. Your guess\
                \ isn't in the word!"
      return (fillInChar puzzle guess)

gameOver :: Puzzle -> IO ()
gameOver (Puzzle w _ guessed) =
  if length guessed >= 5
  then do putStrLn "HA! You lost, madafaka!"
          putStrLn $ "The word was \"" ++ w ++ "\""
          exitSuccess
  else return ()

gameWin :: Puzzle -> IO ()
gameWin (Puzzle w discovered _) =
  if all isJust discovered
  then do putStrLn ("Yeahhhhh! You did it, man :)\
                    \ The word was \"" ++ w ++ "\"")
          exitSuccess
  else return ()
