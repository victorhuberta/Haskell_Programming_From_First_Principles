module Tests where

import Test.Hspec
import Puzzle (Puzzle(..), fillInChar, freshPuzzle)
import HangmanGame (handleGuess)

hangmanTestCase :: IO ()
hangmanTestCase = hspec $ do
  describe "Puzzle.fillInChar" $ do
    it "returns an updated Puzzle with more chars discovered\
        \ if a correct char is given" $ do
      (fillInChar (freshPuzzle "shit") 's')
        `shouldBe`
          (Puzzle "shit" [Just 's', Nothing, Nothing, Nothing] [])

      (fillInChar (freshPuzzle "shit") 'h')
        `shouldBe`
          (Puzzle "shit" [Nothing, Just 'h', Nothing, Nothing] [])

    it "returns an updated Puzzle with more chars guessed\
        \ if an incorrect char is given" $ do
      (fillInChar (freshPuzzle "shit") 'z')
        `shouldBe`
          (Puzzle "shit" [Nothing, Nothing, Nothing, Nothing] ['z'])

      (fillInChar (freshPuzzle "shit") 'x')
        `shouldBe`
          (Puzzle "shit" [Nothing, Nothing, Nothing, Nothing] ['x'])

  describe "HangmanGame.handleGuess" $ do
    it "returns the same puzzle if char has previously been guessed" $ do
      puzzle <- (handleGuess (Puzzle "go" [Nothing, Nothing] ['c']) 'c')
      puzzle `shouldBe` (Puzzle "go" [Nothing, Nothing] ['c'])

    it "returns an updated puzzle if char has not been guessed" $ do
      puzzle <- (handleGuess (Puzzle "go" [Nothing, Nothing] ['z']) 'x')
      puzzle `shouldBe` (Puzzle "go" [Nothing, Nothing] ['z', 'x'])
