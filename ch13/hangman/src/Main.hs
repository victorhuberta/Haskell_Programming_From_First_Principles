module Main where

import System.IO (hSetBuffering, stdout, BufferMode (NoBuffering))
import Data.Char (toLower)
import Tests (hangmanTestCase)

import Puzzle
import RandomWord
import HangmanGame

tests :: IO ()
tests = hangmanTestCase

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  word <- randomWord
  runGame $ freshPuzzle $ fmap toLower word
