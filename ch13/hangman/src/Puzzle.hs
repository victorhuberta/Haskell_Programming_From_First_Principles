module Puzzle where

import Data.List (intersperse)

data Puzzle =
  Puzzle String [Maybe Char] [Char]

instance Show Puzzle where
  show (Puzzle _ discovered guessed) =
    (intersperse ' ' $ fmap renderPuzzleChar discovered)
    ++ " Guessed so far: [" ++ (intersperse ',' guessed) ++ "]"

instance Eq Puzzle where
  (==) (Puzzle w1 d1 g1) (Puzzle w2 d2 g2) =
    w1 == w2 && d1 == d2 && g1 == g2

freshPuzzle :: String -> Puzzle
freshPuzzle w =
  Puzzle w (map (const Nothing) w) []

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle w _ _) c = elem c w

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ discovered guessed) c =
  any alreadyDiscovered discovered || elem c guessed
  where alreadyDiscovered Nothing  = False
        alreadyDiscovered (Just x) = c == x

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar Nothing  = '_'
renderPuzzleChar (Just c) = c

fillInChar :: Puzzle -> Char -> Puzzle
fillInChar (Puzzle w discovered guessed) c =
  if discoverMore /= discovered
  then Puzzle w discoverMore guessed
  else Puzzle w discoverMore addToGuessed
  where compareItems x y z = if x == y then Just y else z
        discoverMore       = zipWith (compareItems c) w discovered
        addToGuessed
          | elem c guessed = guessed
          | otherwise      = guessed ++ [c]
