module PersonMan where

type Name = String
type Age = Int

data Person = Person Name Age deriving (Eq, Show)

data PersonInvalid =
    NameEmpty
  | AgeTooLow
  | PersonInvalidUnknown String
  deriving (Eq, Show)

mkPerson :: Name -> Age -> Either PersonInvalid Person
mkPerson name age
  | name /= "" && age > 0 = Right (Person name age)
  | name == ""            = Left NameEmpty
  | age <= 0              = Left AgeTooLow
  | otherwise             =
      Left $ PersonInvalidUnknown $
             "Name was: " ++ show name ++
             " Age was: " ++ show age

gimmePerson :: IO ()
gimmePerson = do
  putStr "Name: "
  name <- getLine
  putStr "Age: "
  age <- getLine

  let person = mkPerson name (read age)
  case person of
    (Right person) -> do
      putStrLn "Yay! Successfully got a person."
      putStrLn (show person)
    (Left error) -> do
      putStrLn $ "Error: " ++ (show error)
