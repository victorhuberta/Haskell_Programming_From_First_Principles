module Main where

import System.IO

import qualified Hello as H (sayHello)
import Goodbye (byebye)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStr "What is your name?: "
  name <- getLine
  H.sayHello name
  putStrLn "and..."
  byebye name
