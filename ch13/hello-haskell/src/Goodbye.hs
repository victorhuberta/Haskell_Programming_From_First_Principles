module Goodbye (byebye) where

byebye :: String -> IO ()
byebye name = putStrLn ("Goodbye " ++ name ++ "!!")
