module PalindromeMan where

import Control.Monad (forever)
import System.Exit (exitSuccess)
import Data.Char (toLower, isLetter)

main :: IO ()
main = forever $ do
  putStr "Gimme something: "
  something <- getLine
  case isPalindrome something of
    True  -> putStrLn "It's a palindrome!"
    False -> do putStrLn "Nope..."
                exitSuccess

isPalindrome :: String -> Bool
isPalindrome s =
  cleansed == reverse cleansed
  where cleansed = fmap toLower $ filter isLetter s
