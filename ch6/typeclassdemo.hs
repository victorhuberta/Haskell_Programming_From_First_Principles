module TypeClassDemo where


data Mood = Blah | Bleh

-- Manually define a typeclass instance.
instance Show Mood where
  show Blah = "Blah"
  show Bleh = "Bleh"


-- Automatically derive from a typeclass.
data Bit = Zero | One deriving Show


class Numberish a where
  fromNumber :: Integer -> a
  toNumber :: a -> Integer
  defaultNumber :: a


data Age =
  Age Integer
  deriving (Eq, Ord, Show)

instance Numberish Age where
  fromNumber x = Age x
  toNumber (Age x) = x
  defaultNumber = Age 20


data Year =
  Year Integer
  deriving (Eq, Ord, Show)

instance Numberish Year where
  fromNumber x = Year x
  toNumber (Year x) = x
  defaultNumber = Year 1996


sumNumberish :: Numberish a => [a] -> a
sumNumberish [] = fromNumber 0
sumNumberish (x:xs) = fromNumber $ (toNumber x) + (toNumber $ sumNumberish xs)
