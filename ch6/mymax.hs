module MyMax where

myMax :: Ord a => [a] -> a
myMax [x] = x
myMax (x:y:xs) = max (if xs == [] then x else (myMax xs)) (max x y)
