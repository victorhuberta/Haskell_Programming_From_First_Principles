{-# LANGUAGE RankNTypes #-}

module NaturalTrans where

-- This won't typecheck. (->) only accepts * kind types.
--
-- nat :: (f -> g) -> f a -> g b
-- nat = undefined

-- Special syntax: forall
-- We are telling the compiler to ignore all 'a's and
-- only the structures are allowed to be changed.
-- (Nat f g) becomes something like (f -> g).
type Nat f g = forall a . f a -> g a

maybeToList :: Nat Maybe []
maybeToList Nothing  = []
maybeToList (Just x) = [x]
