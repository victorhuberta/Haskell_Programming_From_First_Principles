module TypeCheckLift where

func :: [Int]
func = fmap (+1) $ read "[1]" :: [Int]

func1 :: Maybe [String]
func1 = (fmap . fmap) (++ "lol") (Just ["Hi,", "Hello"])

func2 :: Num a => a -> a
func2 = (*2) . (\x -> x - 2)

func3 :: (Show a, Enum a, Num a) => a -> String
func3 = ((return '1' ++) . show) . (\x -> [x, 1..3])

func4 :: IO Integer
func4 = let ioi = readIO "1" :: IO Integer
            changed = fmap (read . ("123" ++) . show) ioi
        in fmap (*3) changed
