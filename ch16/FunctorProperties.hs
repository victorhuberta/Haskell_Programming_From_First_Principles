module FunctorProperties where

import Test.QuickCheck
import Test.QuickCheck.Function
import Test.QuickCheck.Gen
import Test.QuickCheck.Random

functorIdentity :: (Eq (f a), Functor f) => f a -> Bool
functorIdentity x = fmap id x == x

-- The Fun datatype is used to generate functions.
functorCompose :: (Eq (f c), Functor f)
               => f a -> Fun a b -> Fun b c -> Bool
functorCompose x (Fun _ f) (Fun _ g) =
  (fmap (g . f) x) == (fmap g $ fmap f x)


newtype Identity a = Identity a deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity (f x)

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = frequency [(1, fmap Identity arbitrary)]


data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair x y) = Pair (f x) (f y)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = frequency [(1, do
    pair <- fmap Pair arbitrary
    fmap pair arbitrary)]


data Two a b = Two a b deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two x y) = Two x (f y)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = frequency [(1, do
    two <- fmap Two arbitrary
    fmap two arbitrary)]


data Three a b c = Three a b c deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three x y z) = Three x y (f z)

instance (Arbitrary a, Arbitrary b, Arbitrary c)
      => Arbitrary (Three a b c) where
  arbitrary = frequency [(1, do
    three <- fmap Three arbitrary
    three' <- fmap three arbitrary
    fmap three' arbitrary)]


data Three' a b = Three' a b b deriving (Eq, Show)

instance Functor (Three' a) where
  fmap f (Three' x y z) = Three' x (f y) (f z)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = frequency [(1, do
    three' <- fmap Three' arbitrary
    three'' <- fmap three' arbitrary
    fmap three'' arbitrary)]


data Four a b c d = Four a b c d deriving (Eq, Show)

instance Functor (Four a b c) where
  fmap f (Four w x y z) = Four w x y (f z)

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d)
      => Arbitrary (Four a b c d) where
  arbitrary = frequency [(1, do
    four <- fmap Four arbitrary
    four' <- fmap four arbitrary
    four'' <- fmap four' arbitrary
    fmap four'' arbitrary)]


data Four' a b = Four' a a a b deriving (Eq, Show)

instance Functor (Four' a) where
  fmap f (Four' w x y z) = Four' w x y (f z)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Four' a b) where
  arbitrary = frequency [(1, do
    four' <- fmap Four' arbitrary
    four'' <- fmap four' arbitrary
    four''' <- fmap four'' arbitrary
    fmap four''' arbitrary)]


tests :: IO ()
tests = do
  quickCheck (functorIdentity :: [Int] -> Bool)
  quickCheck (functorCompose :: [Int] -> Fun Int Int -> Fun Int Int -> Bool)
  quickCheck (functorIdentity :: Identity Int -> Bool)
  quickCheck (functorCompose :: Identity Int -> Fun Int Int
                             -> Fun Int Int -> Bool)
  quickCheck (functorIdentity :: Pair Int -> Bool)
  quickCheck (functorCompose :: Pair Int -> Fun Int Int
                             -> Fun Int Int -> Bool)
  quickCheck (functorIdentity :: Two Int Double -> Bool)
  quickCheck (functorCompose :: Two Int Double -> Fun Double Int
                             -> Fun Int Double -> Bool)
  quickCheck (functorIdentity :: Three Int Double String -> Bool)
  quickCheck (functorCompose :: Three Int Double String
                             -> Fun String (Maybe Int)
                             -> Fun (Maybe Int) Double
                             -> Bool)
  quickCheck (functorIdentity :: Three' Int Double -> Bool)
  quickCheck (functorCompose :: Three' Int Double -> Fun Double String
                             -> Fun String Int -> Bool)
  quickCheck (functorIdentity :: Four Int Double String String -> Bool)
  quickCheck (functorCompose :: Four Int Double String String
                             -> Fun String Int -> Fun Int String
                             -> Bool)
  quickCheck (functorIdentity :: Four' Double Int -> Bool)
  quickCheck (functorCompose :: Four' Double Int -> Fun Int Int
                             -> Fun Int String -> Bool)
