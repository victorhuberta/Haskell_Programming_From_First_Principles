module LiftExamples where

replaceWithP :: a -> Char
replaceWithP = const 'p'

lms :: [Maybe [Char]]
lms = [Just "Dance", Nothing, Just "Run"]

replaceWithP' :: [Maybe [Char]] -> Char
replaceWithP' = replaceWithP

liftOnce :: Functor f => f a -> f Char
liftOnce = fmap replaceWithP

liftOnce' :: [Maybe [Char]] -> [Char]
liftOnce' = liftOnce

liftTwice :: (Functor f, Functor f1) => f (f1 a) -> f (f1 Char)
liftTwice = (fmap . fmap) replaceWithP

liftTwice' :: [Maybe [Char]] -> [Maybe Char]
liftTwice' = liftTwice

liftThrice :: (Functor f, Functor f1, Functor f2)
           => f (f1 (f2 a)) -> f (f1 (f2 Char))
liftThrice = (fmap . fmap . fmap) replaceWithP

liftThrice' :: [Maybe [Char]] -> [Maybe [Char]]
liftThrice' = liftThrice

main :: IO ()
main = do
  putStr "replaceWithP' lms:    "
  print (replaceWithP' lms)
  putStr "liftOnce' lms:    "
  print (liftOnce' lms)
  putStr "liftTwice' lms:    "
  print (liftTwice' lms)
  putStr "liftThrice' lms:    "
  print (liftThrice' lms)
