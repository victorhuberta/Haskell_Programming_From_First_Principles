{-# LANGUAGE FlexibleInstances #-}

module FlipFunctor where

data Tuple a b =
  Tuple a b
  deriving (Eq, Show)

data Flip f a b =
  Flip (f b a)
  deriving (Eq, Show)

instance Functor (Flip Tuple a) where
  fmap f (Flip (Tuple y x)) = Flip (Tuple (f y) x)

incFirst :: Flip Tuple String Int
incFirst = fmap (+1) (Flip (Tuple 1 "blah"))
