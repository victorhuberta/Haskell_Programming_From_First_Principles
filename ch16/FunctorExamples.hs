module FunctorExamples where

data ITriedSo a =
    Hard
  | AndLoseItAll Int
  | ButInTheEnd
  | ItDoesntEvenMatter a
  deriving (Eq, Show)

-- Law abiding instance.
-- It has identity, composability, and structure preservation.
-- fmap id x == id x --- holds.
-- fmap (f . g) x == fmap f $ fmap g x --- holds.
instance Functor ITriedSo where
  fmap _ Hard                   = Hard
  fmap _ ButInTheEnd            = ButInTheEnd
  fmap _ (AndLoseItAll x)       = AndLoseItAll x
  fmap f (ItDoesntEvenMatter x) = ItDoesntEvenMatter (f x)

-- Law violating instance.
-- It doesn't have identity and structure preservation.
-- fmap id x == id x --- doesn't hold.
--
-- instance Functor ITriedSo where
--   fmap _ Hard                   = ButInTheEnd
--   fmap _ ButInTheEnd            = Hard
--   fmap f (AndLoseItAll x)       = AndLoseItAll x
--   fmap f (ItDoesntEvenMatter x) = ItDoesntEvenMatter (f x)

-- Law violating instance.
-- It doesn't have composability and structure preservation.
-- fmap (f . g) x == fmap f $ fmap g x --- doesn't hold.
--
-- instance Functor ITriedSo where
--   fmap _ Hard                   = Hard
--   fmap _ ButInTheEnd            = ButInTheEnd
--   fmap f (AndLoseItAll x)       = AndLoseItAll (x+1)
--   fmap f (ItDoesntEvenMatter x) = ItDoesntEvenMatter (f x)
