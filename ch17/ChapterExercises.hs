module ChapterExercises where

import Data.Monoid
import Control.Applicative
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

newtype Identity a = Identity a deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity (f x)

instance Applicative Identity where
  pure x = Identity x
  (Identity f) <*> (Identity x) = Identity (f x)

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = frequency [(1, Identity <$> arbitrary)]

instance Eq a => EqProp (Identity a) where (=-=) = eq


data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair x y) = Pair (f x) (f y)

instance Applicative Pair where
  pure x = Pair x x
  (Pair f f') <*> (Pair x y) = Pair (f x) (f' y)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = frequency [(1, liftA2 Pair arbitrary arbitrary)]

instance Eq a => EqProp (Pair a) where (=-=) = eq


data Two a b = Two a b deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two a b) = Two a (f b)

instance Monoid a => Applicative (Two a) where
  pure x = Two mempty x
  (Two x f) <*> (Two y z) = Two (x <> y) (f z)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = frequency [(1, liftA2 Two arbitrary arbitrary)]

instance (Eq a, Eq b) => EqProp (Two a b) where (=-=) = eq


data Three a b c = Three a b c deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three a b c) = Three a b (f c)

instance (Monoid a, Monoid b) => Applicative (Three a b) where
  pure x = Three mempty mempty x
  (Three x y f) <*> (Three x' y' z) = Three (x <> x') (y <> y') (f z)

instance (Arbitrary a, Arbitrary b, Arbitrary c)
      => Arbitrary (Three a b c) where
  arbitrary = frequency [(1, liftA3 Three arbitrary arbitrary arbitrary)]

instance (Eq a, Eq b, Eq c) => EqProp (Three a b c) where (=-=) = eq


data Three' a b = Three' a b b deriving (Eq, Show)

instance Functor (Three' a) where
  fmap f (Three' a b b') = Three' a (f b) (f b')

instance Monoid a => Applicative (Three' a) where
  pure x = Three' mempty x x
  (Three' x f f') <*> (Three' x' y z) = Three' (x <> x') (f y) (f' z)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = frequency [(1, liftA3 Three' arbitrary arbitrary arbitrary)]

instance (Eq a, Eq b) => EqProp (Three' a b) where (=-=) = eq


data Four a b c d = Four a b c d deriving (Eq, Show)

instance Functor (Four a b c) where
  fmap f (Four a b c d) = Four a b c (f d)

instance (Monoid a, Monoid b, Monoid c) => Applicative (Four a b c) where
  pure x = Four mempty mempty mempty x
  (Four a b c f) <*> (Four a' b' c' d) =
    Four (a <> a') (b <> b') (c <> c') (f d)

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d)
      => Arbitrary (Four a b c d) where
  arbitrary =
    frequency [(1, liftA3 Four arbitrary arbitrary arbitrary <*> arbitrary)]

instance (Eq a, Eq b, Eq c, Eq d) => EqProp (Four a b c d) where (=-=) = eq


data Four' a b = Four' a a a b deriving (Eq, Show)

instance Functor (Four' a) where
  fmap f (Four' a a' a'' b) = Four' a a' a'' (f b)

instance Monoid a => Applicative (Four' a) where
  pure x = Four' mempty mempty mempty x
  (Four' x y z f) <*> (Four' x' y' z' b) =
    Four' (x <> x') (y <> y') (z <> z') (f b)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Four' a b) where
  arbitrary =
    frequency [(1, liftA3 Four' arbitrary arbitrary arbitrary <*> arbitrary)]

instance (Eq a, Eq b) => EqProp (Four' a b) where (=-=) = eq


main :: IO ()
main = do
  quickBatch $  applicative (undefined :: Identity (Int, String, Double))
  quickBatch $ applicative (undefined :: Pair (Char, Int, Double))
  quickBatch $ applicative (undefined :: Two [Int] (String, Double, Int))
  quickBatch $
    applicative (undefined :: Three [Int] [Char] (Double, String, Int))
  quickBatch $ applicative (undefined :: Three' [Double] (Char, Int, String))
  quickBatch $
    applicative (undefined :: Four [Int] [Char] [Double] (Char, Int, String))
  quickBatch $ applicative (undefined :: Four' [Double] (Char, Int, String))
