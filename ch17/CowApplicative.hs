module CowApplicative where

import Control.Applicative

data Cow = Cow {
      name   :: String
    , age    :: Int
    , weight :: Int
  } deriving (Eq, Show)


noEmpty :: String -> Maybe String
noEmpty "" = Nothing
noEmpty s  = Just s

noNegative :: Int -> Maybe Int
noNegative x
  | x < 0     = Nothing
  | otherwise = Just x

mkCow :: String -> Int -> Int -> Maybe Cow
mkCow name age weight =
  Cow <$> noEmpty name
      <*> noNegative age
      <*> noNegative weight

mkCow' :: String -> Int -> Int -> Maybe Cow
mkCow' name age weight =
  liftA3 Cow (noEmpty name)
             (noNegative age)
             (noNegative weight)
