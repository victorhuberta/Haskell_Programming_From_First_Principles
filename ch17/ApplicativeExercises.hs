module ApplicativeExercises where

newtype Identity a = Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity (f x)

instance Applicative Identity where
  pure x = Identity x
  (<*>) (Identity f) (Identity y) = Identity (f y)


newtype Constant a b =
  Constant { getConstant :: a }
  deriving (Eq, Show)

instance Functor (Constant a) where
  fmap _ (Constant x) = Constant x

instance Monoid a => Applicative (Constant a) where
  pure _ = Constant mempty
  (<*>) (Constant x) (Constant y) = Constant (x `mappend` y)


constJust :: Maybe String
constJust = const <$> Just "Hello" <*> pure "World"

tupleness :: Maybe (Int, Int, [Char], [Int])
tupleness =
  (,,,) <$> Just 90
        <*> Just 10
        <*> Just "Tierness"
        <*> pure [1, 2, 3]
