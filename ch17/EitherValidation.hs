module EitherValidation where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)

data Validation e a =
    Error e
  | Success' a
  deriving (Eq, Show)

instance Functor (Sum a) where
  fmap f (First x) = First x
  fmap f (Second x) = Second (f x)

instance Applicative (Sum a) where
  pure x = Second x
  (First x) <*> _ = First x
  _ <*> (First x) = First x
  (Second f) <*> (Second y) = Second (f y)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Sum a b) where
  arbitrary = frequency [ (1, First <$> arbitrary)
                        , (1, Second <$> arbitrary) ]

instance (Eq a, Eq b) => EqProp (Sum a b) where
  (=-=) = eq

instance Functor (Validation e) where
  fmap f (Error x) = Error x
  fmap f (Success' x) = Success' (f x)

instance Monoid e => Applicative (Validation e) where
  pure x = Success' x
  (Error x)   <*> (Error y)   = Error (x `mappend` y)
  (Error x)   <*> _           = Error x
  _           <*> (Error x)   = Error x
  (Success' f) <*> (Success' y) = Success' (f y)

instance (Arbitrary e, Arbitrary a) => Arbitrary (Validation e a) where
  arbitrary = frequency [ (1, Error <$> arbitrary)
                        , (1, Success' <$> arbitrary) ]

instance (Eq e, Eq a) => EqProp (Validation e a) where
  (=-=) = eq

main :: IO ()
main = do
  quickBatch $ functor (undefined :: Sum Int (Int, Char, String))
  quickBatch $ applicative (undefined :: Sum Int (Int, Char, String))
  quickBatch $ functor (undefined :: Validation Int (String, Int, Double))
  quickBatch $ applicative (
    undefined :: Validation [Int] (String, Int, Double))
