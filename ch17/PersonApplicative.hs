module PersonApplicative where

validateLength :: Int -> String -> Maybe String
validateLength maxlen s =
  if (length s) > maxlen
  then Nothing
  else Just s


newtype Name = Name String
  deriving (Eq, Show)

mkName :: String -> Maybe Name
mkName s = Name <$> validateLength 25 s


newtype Age = Age Int
  deriving (Eq, Show)

mkAge :: Int -> Maybe Age
mkAge x = if x < 1 then Nothing else Just (Age x)


newtype Address = Address String
  deriving (Eq, Show)

mkAddress :: String -> Maybe Address
mkAddress s = Address <$> validateLength 40 s


data Person = Person Name Age Address
  deriving (Eq, Show)

mkPerson :: String -> Int -> String -> Maybe Person
mkPerson name age addr =
  Person <$> mkName name <*> mkAge age <*> mkAddress addr
