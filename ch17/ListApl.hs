module ListApl where

import Data.Monoid
import Control.Applicative
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)

instance Monoid (List a) where
  mempty = Nil
  mappend Nil          list        = list
  mappend list         Nil         = list
  mappend (Cons x Nil) (Cons y ys) = Cons x (Cons y ys)
  mappend (Cons x xs)  list        = Cons x (xs <> list)

instance Functor List where
  fmap _ Nil         = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Applicative List where
  pure x = Cons x (Nil)
  Nil         <*> _    = Nil
  _           <*> Nil  = Nil
  (Cons f fs) <*> list = (f <$> list) <> (fs <*> list)

instance Eq a => EqProp (List a) where
  (=-=) = eq

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = frequency [ (1, return Nil)
                        , (1, liftA2 Cons arbitrary arbitrary)]


newtype ZipList' a =
  ZipList' (List a)
  deriving (Eq, Show)

--instance Eq a => EqProp (ZipList' a) where
--  xs =-= ys = xs' `eq` ys'
--    where xs' = let (ZipList' l) = xs
--                in take' 3000 l
--          ys' = let (ZipList' l) = ys
--                in take' 3000 l

-- This works fine (???)
instance Eq a => EqProp (ZipList' a) where
  (=-=) = eq

instance Functor ZipList' where
  fmap f (ZipList' xs) = ZipList' (fmap f xs)

instance Applicative ZipList' where
  pure = ZipList' . pure
  (ZipList' fs) <*> (ZipList' xs) = ZipList' $ fs <*> xs

instance Arbitrary a => Arbitrary (ZipList' a) where
  arbitrary = frequency [ (1, return $ ZipList' Nil)
                        , (1, ZipList' <$> arbitrary)]

take' :: Int -> List a -> List a
take' _ Nil = Nil
take' 0 _   = Nil
take' n (Cons x xs) = Cons x $ take' (n - 1) xs

append :: List a -> List a -> List a
append Nil ys = ys
append (Cons x xs) ys = Cons x $ xs `append` ys

fold :: (a -> b -> b) -> b -> List a -> b
fold _ b Nil = b
fold f b (Cons x xs) = f x (fold f b xs)

concat' :: List (List a) -> List a
concat' = fold append Nil

flatMap :: (a -> List b) -> List a -> List b
flatMap f = concat' . fmap f

testsForListApl :: IO ()
testsForListApl =
  quickBatch $ applicative (undefined :: List (Int, Char, Double))

testsForZipListApl :: IO()
testsForZipListApl =
  quickBatch $ applicative (undefined :: ZipList' (Int, Char, Double))
